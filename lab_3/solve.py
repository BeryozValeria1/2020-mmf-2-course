from math import sqrt

def findSide(x, y, x1, y1):
    return sqrt((int(x) - int(x1)) ** 2 + (int(y) - int(y1)) ** 2)


def perimeterPolygon(arr):
    if len(arr) < 3:
        return False

    firstPointX = arr[0][0]
    firstPointY = arr[0][1]

    pointX = arr[1][0]
    pointY = arr[1][1]

    nextPointX = arr[2][0]
    nextPointY = arr[2][1]

    ans = findSide(firstPointX, firstPointY, pointX, pointY)

    for i in range(2, len(arr)):
        ans += findSide(pointX, pointY, nextPointX, nextPointY)

        pointX = nextPointX
        pointY = nextPointY

        if (len(arr[i]) == 0):
            continue

        nextPointX = arr[i][0]
        nextPointY = arr[i][1]

    ans += findSide(pointX, pointY, firstPointX, firstPointY)

    return ans



        
